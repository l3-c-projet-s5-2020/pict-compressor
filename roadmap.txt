TODO
change black and white image -> quadtree    fromat encodage .qtn à ouvrir, lire et écrire en bit à bit
change color image -> quadtree              format encodage .qtc à ouvrir, lire et écrire en bit à bit
change quedtree -> image

* quadtree : 
- parcour en profondeur préfixe 
- noeud interne = 0
- en noir et blanc:
    - feuille = 1 + coueleur(1 = noir, 0 = blanc)
- en couleur:
    - feuille = 1 + 4 octets(red, green, bleu, alpha(opacité))

* minimisation des quadtree:
- encoder comme un graph orineté acyclique enraciné:
    -chq noeud possède un numéro:
        en noir et blanc voir exemple echiquier                             extension .gmn
        en couleur [numérofeuille]f r(0-255) g(0-255) b(0-255) a(0-255)     extension .gmc
- 1/ sans perte
- 2/ avec perte 
- comparer :
    - zip d'une image(jpg, png, ...)
    - zip du quadtree(en sauvegarde binaire)
    - zip sauvegarde sémantique graph minimiseé

* but prog:
- ouvrir image en 512x512 de format comme dans MLV (png, jpg,gif, tiff,pcx,tga, etc...)
- penser à garder l'image de base affiché pour comparer.
- boutons :
    - change l'image en quadtree
    - sauvegarde binaire en noir et blanc
    - sauvegarde binaire en rgba
    - lancer la minimisation du quadtree
    - sauvegarder le graph mini en nb
    - sauvegarder le graph mini en rgba
    - boutons pour ouvrir une image en fct de son extension (qtn, qtc, gmn, gmc, format habituel(png, jpg, gif, etc...))
